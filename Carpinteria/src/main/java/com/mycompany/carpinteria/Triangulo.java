/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.carpinteria;

/**
 *
 * @author Joe
 */
public class Triangulo extends Formas {

    private double base;
    private double altura;

    public Triangulo(double altura, double base) {
        this.base = base;
        this.altura = altura;
    }

    public double calcularArea() {
        return (base * altura)/2;
    }
}
