/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.carpinteria;

/**
 *
 * @author Joe
 */
public class Cuadrado extends Formas {

    private double lado;

    public Cuadrado(double lado) {
        this.lado = lado;

    }

    public double calcularArea() {
        return lado * lado;
    }

}
