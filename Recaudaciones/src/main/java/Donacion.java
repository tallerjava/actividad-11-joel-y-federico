
public class Donacion implements Comparable<Donacion> {

    private String pais;
    private Double monto;

    public Donacion(String pais, Double monto) {
        this.pais = pais;
        this.monto = monto;
    }

    public String getPais() {
        return pais;
    }

    public Double getMonto() {
        return monto;
    }

    public int compareTo(Donacion d) {
        if (monto < d.monto) {
            return -1;
        }
        if (monto > d.monto) {
            return 1;
        }
        return 0;
    }
}
